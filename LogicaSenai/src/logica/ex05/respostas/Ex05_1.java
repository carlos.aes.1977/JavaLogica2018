package logica.ex05.respostas;

import logica.lib.Util;

public class Ex05_1 {
	public static void main(String[] args) {
		double valor = Util.leReal("Informe o valor");
		double media = valor;
		int cnt = 0;
		
		for(;valor > 0;cnt++) {
			valor = Util.leReal("Informe o valor");
			media += valor;
		}
		Util.escreval("A média é: ", media / cnt);
	}
}
