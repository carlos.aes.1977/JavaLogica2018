package logica.ex05.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex03 {
	public static void main(String[] args) {
		int total = 0;
		for (int i = 0; i < 5; i++) {
			total += Util.leInteiro("Informe um nº");
		}
		
		JOptionPane.showMessageDialog(null, 
				String.format("A soma: %d%nA média: %,.2f ", total, total / 5d));
	}
}
