package logica.ex05.respostas;

import logica.lib.Util;

public class Ex05_2 {
	public static void main(String[] args) {
		int analista = 0;
		int programador = 0;
		int usuario = 0;
		int gestor = 0;
		int outro = 0;
		
		String cargo = Util.leTexto("Informe o cargo");
		
		while(!cargo.equalsIgnoreCase("fim")) {
			switch (cargo.toLowerCase()) {
			case "analista":
				analista++;
				break;
			case "programador":
				programador++;
				break;
			case "usuario":
				usuario++;
				break;
			case "gestor":
				gestor++;
				break;
			default:
				outro++;
				break;
			}
			
			cargo = Util.leTexto("Informe o cargo");
		}
		
		Util.escreval("Analistas: ", analista,
					  "\nProgramador: ", programador,
				  	  "\nUsuário: ", usuario,
				 	  "\nGestor: ", gestor,
				 	  "\nOutro: ", outro);
	}
}
