package logica.ex05.respostas;

import static logica.lib.Util.escreval;
import static logica.lib.Util.leInteiro;

public class Ex06 {
	public static void main(String[] args) {
		int num = leInteiro("Informe o nº");
		int oMenor = num;
		int cnt = 0;
		
		while(num > 0) {
			if(num < oMenor) {
				oMenor = num;
				cnt = 1;
			} else if(num == oMenor) {
				cnt++;
			}
			
			num = leInteiro("Informe o nº");
		}
		
		escreval("O Menor nº informado é:", oMenor, "\ne foi informado ", cnt, " vezes");
	}
}
