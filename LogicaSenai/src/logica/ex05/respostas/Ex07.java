package logica.ex05.respostas;

import logica.lib.Util;

public class Ex07 {
	public static void main(String[] args) {
		double oMaior1 = 0;
		double oMaior2 = 0;
		double oMaior3 = 0;
		
		int sal = Util.leInteiro("Informe o Salário");
		
		while(sal > 0) {
			if(sal >= oMaior1) {
				if(sal > oMaior1) {
					oMaior3 = oMaior2;
					oMaior2 = oMaior1;
					oMaior1 = sal;
				}
			} else if(sal >= oMaior2) {
				if(sal > oMaior2) {
					oMaior3 = oMaior2;
					oMaior2 = sal;
				}
			} else if( sal > oMaior3) {
				oMaior3 = sal;
			}
			
			Util.escreval("\nO 1º Salário: ", oMaior1,
						  "\nO 2º Salário: ", oMaior2,
						  "\nO 3º Salário: ", oMaior3);
			
			sal = Util.leInteiro("Informe o Salário");
		}
		
		Util.escreval("\nO 1º Salário: ", oMaior1,
					  "\nO 2º Salário: ", oMaior2,
					  "\nO 3º Salário: ", oMaior3);
	}
}
