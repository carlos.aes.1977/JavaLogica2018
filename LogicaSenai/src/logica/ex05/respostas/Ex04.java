package logica.ex05.respostas;

import static logica.lib.Util.leInteiro;

import javax.swing.JOptionPane;

public class Ex04 {
	public static void main(String[] args) {
		int qtd =  leInteiro("Informe a quantidade");
		
		if(qtd > 0) {
			int oMaior = 0;
			
			for (int i = 0; i < qtd; i++) {
				int num = leInteiro("Informe um nº");
				
			    if(i == 0) {
			    	oMaior = num;
			    } else if(num > oMaior) {
					oMaior = num;
				}	
			}
			
			JOptionPane.showMessageDialog(null,"O maior: " + oMaior);
		} else {
			JOptionPane.showMessageDialog(null, "Nenhum valor foi informado");
		}
	}
}
