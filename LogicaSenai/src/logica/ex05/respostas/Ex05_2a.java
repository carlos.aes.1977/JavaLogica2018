package logica.ex05.respostas;

import logica.lib.Util;
/*
 
 Apresente a contagem das pessoas que ingressam no congresso 
 com base no critério a seguir:
• analista
• programador • usuário
• gestor
• outro
 
 */

public class Ex05_2a {
	public static void main(String[] args) {
		// Declara um vetor para guardar os contadores de cargos
		int[] cnt = {0, 0, 0, 0, 0};
		
		// Declara as constantes com a posição dos campos de cargo
		// no vetor bidimensdional abaixo
		final int CARGOS = 0;
		final int TITULOS_DOS_CARGOS = 1;
		
		// Vetor bidimensional contendo o os nomes de cargos
		String[][] cargos = {
			//     Nome usado    Titulo usado
			//     no teste	     no relatorio
				{ "analista",    "Analista"},
				{ "programador", "Programador"},
				{ "usuario",     "Usuário"},
				{ "gestor",      "Gestor"},
				{ "outro",       "Outro"}	
		};
			
		String oCargo = Util.leTexto("Informe o cargo");
		
		// Lê os cargos enquanto for diferente de "fim"
		while(!oCargo.equalsIgnoreCase("fim")) {
			
			// Procura O Cargo informado no vetor de cargos
			for (int i = 0; i < cargos.length; i++) {
			    String[] cargo = cargos[i];
			    
			    // Se encontrar o cargo na lista
				if(cargo[CARGOS].equalsIgnoreCase(oCargo)) {
					// acrescenta 1 ao respectivo contador
					cnt[i]++;
				}
			}
				
			oCargo = Util.leTexto("Informe o cargo");
		}
		
		// Define um titulo para o relatorio
		String msg = "Relatório Final\n";
		
		// Percorre a lista de cargos
		for (int i = 0; i < cargos.length; i++) {
			// e para cada titulo de cargo
			String[] cargo = cargos[i];
			
			// e para cada cargo fortatar o titulo de cargo e seu respetino contador
			msg += Util.formata("\n", cargo[TITULOS_DOS_CARGOS], ": ", cnt[i]);
		}
		
		Util.escreval(msg);
	}
}
