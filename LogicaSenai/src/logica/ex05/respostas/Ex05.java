package logica.ex05.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex05 {
	public static void main(String[] args) {
		double sal = Util.leInteiro("Informe o Salário");
		
		while(sal > 0) {
			if(sal <= 500) {
				sal *= 1.2;
			} else {
				sal *= 1.1;
			}
			JOptionPane.showMessageDialog(null, String.format("O Salário com aumento é de: %,.2f", sal));
			
			
			sal = Util.leInteiro("Informe o Salário");
		}
	}
}
