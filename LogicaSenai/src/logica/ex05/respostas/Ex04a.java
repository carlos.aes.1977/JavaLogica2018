package logica.ex05.respostas;

import static logica.lib.Util.leInteiro;

import javax.swing.JOptionPane;

public class Ex04a {
	public static void main(String[] args) {
		int qtd =  leInteiro("Informe a quantidade");
		
		if(qtd > 0) {
			int oMaior = Integer.MIN_VALUE;
			
			for (int i = 0; i < qtd; i++) {
				int num = leInteiro("Informe um nº");
			
				if(num > oMaior) {
					oMaior = num;
				}	
			}
			
			JOptionPane.showMessageDialog(null,"O maior: " + oMaior);
		} else {
			JOptionPane.showMessageDialog(null, "Nenhum valor foi informado");
		}
	}
}
