package logica.ex05.exemplos;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex01 {
	public static void main(String[] args) {
		double media = 0;
		
		for (int i = 0; i < 4; i++) {
			media += Util.leInteiro("Informe a "+(i+1)+"ª nota");
		}
		
		media /= 4d;
		
		String msg = "Reprovado";
		if(media >= 7) {
			msg = "Aprovado";
		}
		
		msg = String.format("A Média é: %,.2f\n%s", media, msg);
		
		JOptionPane.showMessageDialog(null, msg);
	}
}
