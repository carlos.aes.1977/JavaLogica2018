package logica.ex05.exemplos;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Contador2 {
	public static void main(String[] args) {
		for(int num = Util.leInteiro("Informe um numero");
				num > 0; 
				num = Util.leInteiro("Informe um numero")) {
			num *= 10;
			JOptionPane.showMessageDialog(null, num);
		}
	}
}
