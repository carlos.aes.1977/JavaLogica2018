package logica.ex07.exemplos;

import logica.lib.Util;

public class Fibonacci {
	public static void main(String[] args) {
		int num = Util.leInteiro("Informe um nº");

		while (num > 0) {
			Util.escreval(f(num));

			num = Util.leInteiro("Informe um nº");
		}
	}

	public static int f(int n) {
		if (n > 2) {
			return f(n - 2) + f(n - 1);
		}
		return 1;
	}

}
