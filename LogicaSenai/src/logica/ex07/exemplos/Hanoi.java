package logica.ex07.exemplos;

import logica.lib.Util;

public class Hanoi {
	public static void main(String[] args) {
		int num = Util.leInteiro("Informe um nº");
		
		Util.escreval( hanoi(num) );
	}

	public static int hanoi(int n) {
		if(n > 1) {
			return 2 * hanoi(n - 1) + 1;
		}
		return 1;
	}
	
	
}
