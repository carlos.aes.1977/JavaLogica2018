package logica.ex07.respostas;

import logica.lib.Util;

public class Ex03 {
	public static void main(String[] args) {
		int num = Util.leInteiro("Informe o nº da conta");
		
		while(num > 0) {
			Util.escreval("O dígito verificador para a conta ", num, " é " , digito(num));
			
			num = Util.leInteiro("Informe o nº da conta");
		}
	}
	
	public static int digito(int conta) {
		int soma = conta + Ex02.inverte(conta);
		
		int dig = 0;
		for (int i = 6; i >= 1; i--) {
			dig += soma % 10 * i;
			soma /= 10;
		}
		
		return dig % 10;
	}
}
