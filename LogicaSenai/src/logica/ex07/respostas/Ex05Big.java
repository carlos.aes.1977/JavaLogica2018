package logica.ex07.respostas;

import java.math.BigInteger;

import logica.lib.Util;

public class Ex05Big {
	public static void main(String[] args) {
		String num = Util.leTexto("Informe o nº para o cálculo do Fatorial");
		
		while(!num.equalsIgnoreCase("fim")) {
			System.out.println("O valor calculado com a função fatorial para\n" + num +
					"\né: " + fatorial(new BigInteger(num)));
			
			num = Util.leTexto("Informe o nº para o cálculo do Fatorial");
		}
	}
	
	public static BigInteger fatorial(BigInteger num) {
		if(num.compareTo(BigInteger.ONE) > 0)
			return num.multiply(fatorial(num.subtract(BigInteger.ONE)));
		
		return BigInteger.ONE;
	}
}
