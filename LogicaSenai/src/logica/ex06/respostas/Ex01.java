package logica.ex06.respostas;

import java.util.Arrays;

import logica.lib.Util;

public class Ex01 {
	public static void main(String[] args) {
		int[] num = new int[10];
		double media = 0;
		
		for (int i = 0; i < num.length; i++) {
			num[i] = Util.leInteiro("Informe um nº");
			media += num[i];
		}
		media /= num.length;
		
		Arrays.sort(num);
		
		String msg = String.format("Números maiores que a média: %,.2f\n\n", media);
		for (int i = 0; i < num.length; i++) {
			if(num[i] > media) {
				msg += num[i] + " ";
			}
		}
		Util.escreval(msg);
	}
}
