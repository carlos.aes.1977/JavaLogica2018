package logica.ex06.exemplos;

public class ComoOrdenar {
	public static void main(String[] args) {		
		int[] num = {
				12, 3, 15, 2, 8, 6, 3, 20, 19, 4
		};

		boolean trocou;
		do {
			trocou = false;
			for (int i = 0; i < num.length - 1; i++) {
				if (num[i] > num[i + 1]) {
					int temp = num[i];
					num[i] = num[i + 1];
					num[i + 1] = temp;
					trocou = true;
				}
			}
			
			for (int i = 0; i < num.length; i++) {
				System.out.print(num[i] + ", ");
			}
			System.out.println("\n" + (trocou ? "não" : "") + " está em Ordem");
		} while(trocou);
		
		for (int i = num.length -1; i >= 0 ; i--) {
			System.out.print(num[i] + ", ");
		}
		
		
	}
}
