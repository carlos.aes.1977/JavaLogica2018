package logica.ex03.exemplos;

import javax.swing.JOptionPane;

public class ExemploCondicoes {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe a idade");
		
		int idade = Integer.parseInt(temp);

		String msg = "";
		
		if(idade >= 18 ) {
			// verdade
			msg = "Vá trabalhar!";
		} else {
			// falsa
			msg = "Vá para a escola";
		} 
		
		JOptionPane.showMessageDialog(null, msg);
	}
}
