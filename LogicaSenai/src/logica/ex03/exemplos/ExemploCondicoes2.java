package logica.ex03.exemplos;

import javax.swing.JOptionPane;

public class ExemploCondicoes2 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe a idade");
		int idade = Integer.parseInt(temp);
		
		String sexo = JOptionPane.showInputDialog("Informe o sexo").toLowerCase();

		String msg = "";
		
		if(idade >= 18 && sexo.startsWith("m")) {
			msg = "Vá trabalhar!";
		} else if(idade >= 18 && sexo.startsWith("f")) {
			msg = "Vá ajudar a sua mãe!";
		} else {
			msg = "Vá para a escola";
		} 
		
		JOptionPane.showMessageDialog(null, msg);
	}
}
