package logica.ex03.exemplos;

import javax.swing.JOptionPane;



public class ExemploCondicoes5 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe o mês");
		int mes = Integer.parseInt(temp);

		String msg = "";

		if (mes >= 1 && mes <= 6) {
			msg = "1º Semestre";
		} else if(mes >= 7 && mes <= 12) {
			msg = "2º semestre";
		} else {
			msg = "Mês inválido";
		}
	
		JOptionPane.showMessageDialog(null, msg);
	}
}
