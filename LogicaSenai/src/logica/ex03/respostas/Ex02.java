package logica.ex03.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex02 {
	public static void main(String[] args) {
		int num = Util.leInteiro("Informe um  nº");
		
		String resp = "É impar";
		if(num % 2 == 0)
			resp = "É par";
		
		JOptionPane.showMessageDialog(null, resp);
	}
}
