package logica.ex03.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex04 {
	public static void main(String[] args) {
		double sal = Util.leReal("Informe o sal Bruto");
		
		if(sal < 300) {
			sal *= .95;
		} else if(sal <= 1200) {
			sal *= .9;
		} else {
			sal *= .85;
		}
		
		JOptionPane.showMessageDialog(null, 
				String.format("O sal Liq é de R$ %,.2f", sal));
	}
}
