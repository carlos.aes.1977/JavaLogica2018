package logica.ex03.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex02a {
	public static void main(String[] args) {
		int num = Util.leInteiro("Informe um  nº");
		
		String resp = num % 2 == 0 ? "É par" : "É impar";
		
		JOptionPane.showMessageDialog(null, resp);
	}
}
