package logica.ex03.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex05 {
	public static void main(String[] args) {
		int num1 = Util.leInteiro("Informe o 1º nº");
		int num2 = Util.leInteiro("Informe o 2º nº");

		String resp;
		if(num1 > num2) {
			resp = num2 + " " + num1;
		} else {
			resp = num1 + " " + num2;
		}
		
		JOptionPane.showMessageDialog(null, resp);
	}
}
