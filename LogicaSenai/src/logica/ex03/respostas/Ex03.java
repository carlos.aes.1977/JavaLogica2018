package logica.ex03.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex03 {
	public static void main(String[] args) {
		int A = Util.leInteiro("Informe o valor de A");
		int B = Util.leInteiro("Informe o valor de B");
		
		String resp;
		
		if(A % B == 0) {
			resp = "É divisível";
		} else {
			resp = "Não é divisível";
		}
		
		JOptionPane.showMessageDialog(null, resp);
	}
}
