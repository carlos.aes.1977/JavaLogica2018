package logica.ex02;

import javax.swing.JOptionPane;

public class Exercicio1 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe o valor do Saque");
		
		int saque = Integer.parseInt(temp);
		
		String msg = "Notas de 100: " + saque / 100;
		saque %= 100; // saque = saque % 100;
		
		msg += "\nNotas de 50: " + saque / 50;
		saque %= 50; 
		
		msg += "\nNotas de 20: " + saque / 20;
		saque %= 20; 
		
		msg += "\nNotas de 10: " + saque / 10;
		saque %= 10; 
		
		msg += "\nNotas de 5: " + saque / 5;
		saque %= 5; 
		
		msg += "\nNotas de 2: " + saque / 2;
		saque %= 2; 
		
		msg += "\nNotas de 1: " + saque;
	
		JOptionPane.showMessageDialog(null, msg);
	}
}
