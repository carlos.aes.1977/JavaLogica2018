package logica.ex02;

import javax.swing.JOptionPane;

public class Exercicio2a {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe a 1ª nota");
		double nota1 = Double.parseDouble(temp);

		temp = JOptionPane.showInputDialog("Informe a 2ª nota");
		double nota2 = Double.parseDouble(temp);

		temp = JOptionPane.showInputDialog("Informe a 3ª nota");
		double nota3 = Double.parseDouble(temp);

		temp = JOptionPane.showInputDialog("Informe a 4ª nota");
		double nota4 = Double.parseDouble(temp);
		
		double total = (nota1 + nota2 + nota3 + nota4) / 4;
		
		String msg = String.format("A média é de: %,.2f" , total);
		
		JOptionPane.showMessageDialog(null, msg);
	}
}
