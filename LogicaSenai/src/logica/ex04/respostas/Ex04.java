package logica.ex04.respostas;

import javax.swing.JOptionPane;

import logica.lib.Util;

public class Ex04 {
	public static void main(String[] args) {
		String nome = JOptionPane.showInputDialog("Informe o Nome");
		int nota1 = Util.leInteiro("Informe a 1ª nota");
		int nota2 = Util.leInteiro("Informe a 2ª nota");
		int nota3 = Util.leInteiro("Informe a 3ª nota");
		int faltas = Util.leInteiro("Informe o nº de faltas");
		
		double media = (nota1 + nota2 + nota3) / 3;
		
		String situacao;
		if(faltas > 15) {
			situacao = "Reprovado por Faltas";
		} else if(media < 7) {
			situacao = "Reprovado por Média";
		} else {
			situacao = "Aprovado";
		}
		
		JOptionPane.showMessageDialog(null, nome + ", você foi: " + situacao);
	}
}
